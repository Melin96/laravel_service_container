<?php
declare(strict_types=1);

namespace UserFeed\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ComplaintTypeResource
 * @package UserFeed\Http\Resources
 */
class SubscribersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->name,
        ];
    }

}
