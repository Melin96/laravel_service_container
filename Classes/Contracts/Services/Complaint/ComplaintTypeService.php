<?php
declare(strict_types=1);

namespace UserFeed\Classes\Contracts\Services\Complaint;

use Illuminate\Database\Eloquent\Collection;


/**
 * Interface ComplaintTypeService
 * @package UserFeed\Classes\Contracts\Services\Complaint
 */
interface ComplaintTypeService
{

    /**
     * @return Collection|null
     */
    public function getAllTypes(): ?Collection;

}
